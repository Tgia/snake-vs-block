﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUp : MonoBehaviour
{
    [SerializeField]
    private int lpick;
    
    private int randomizePick()
    {
        lpick = Random.Range(1, 5);
        return lpick;
    }

    public int Lpick
    {
        get { return randomizePick(); }
    }
}