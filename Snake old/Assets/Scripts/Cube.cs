﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cube : MonoBehaviour
{
   
    private int lifeSmasher;

 

    private int randomizeSmash()
    {
        lifeSmasher = Random.Range(1, 10);
        return lifeSmasher;
    }

    public int LifeSmasher
    {
        get { return randomizeSmash(); }
    }
    
}
