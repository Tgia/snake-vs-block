﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Controllers : MonoBehaviour
{
    private static int lifeCount = 4;

    private float speed = 0.5f;
    private PickUp currentPick;
    private Cube currentCube;
    private TextMesh life;
    private TextMesh cubeLife;
    private TextMesh pickLife;
    private bool is_themoment = false;
    private bool needWait = false;
    private int auxLifeSmash;
    private int auxPick;
    private GameObject help;
    private void Awake()
    {
        currentCube = GameObject.FindWithTag("Cube").GetComponent<Cube>();
        currentPick = GameObject.FindWithTag("LifePick").GetComponent<PickUp>();
        cubeLife = GameObject.FindWithTag("Number").GetComponent<TextMesh>();
        pickLife = GameObject.FindWithTag("Lifenode").GetComponent<TextMesh>();
        life = GameObject.FindWithTag("Life").GetComponent<TextMesh>();
        life.text = "4";
        auxLifeSmash = currentCube.LifeSmasher;
        auxPick = currentPick.Lpick;
        cubeLife.text = auxLifeSmash.ToString();
        pickLife.text = auxPick.ToString();
        Debug.Log("player: " + lifeCount.ToString());
        Debug.Log("control Pick: " + auxPick.ToString());
        Debug.Log("control cube: " + auxLifeSmash.ToString());
        
    }

    
        private void Update()
    {
        if (needWait)
        {
            speed = 0;
            if (lifeCount > 0 && auxLifeSmash > 0)
            {
                auxLifeSmash--;
                lifeCount--;
                life.text = lifeCount.ToString();
                cubeLife.text = auxLifeSmash.ToString();
            }
            if (lifeCount != 0)
            {
                speed = 0.5f;
            }
            else
            {
                SceneManager.LoadScene("Lose");
            }
        }
    }

    private void FixedUpdate()
    {
       
        transform.position += new Vector3(Input.GetAxis("Horizontal"), speed * Time.deltaTime, 0);

        if (transform.position.x < -3) { 
            transform.position = new Vector3(-3, transform.position.y);
        }
        else if (transform.position.x > 3)
        {
            transform.position = new Vector3(3, transform.position.y);
        }
        if (is_themoment)
        {
            currentPick.transform.position = new Vector3(transform.position.x, transform.position.y - 0.23f, transform.position.z);
        }
        
    }

    private void LateUpdate()
    {
        Camera.main.transform.position = new Vector3(Camera.main.transform.position.x, transform.position.y, Camera.main.transform.position.z);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Cube"))
        {
            needWait = true;
            other.gameObject.SetActive(false);
            Debug.Log("player: " + lifeCount.ToString());
            Debug.Log("control cube: " + auxLifeSmash.ToString());
            

        }
        if (other.gameObject.CompareTag("LifePick"))
        {
            lifeCount = lifeCount + auxPick;
            is_themoment = true;
            life.text = lifeCount.ToString();
            Debug.Log("control Pick: " + lifeCount.ToString());
        }
        if (other.gameObject.CompareTag("New"))
        {
            SceneManager.LoadScene("Win");
        }
    }
}
