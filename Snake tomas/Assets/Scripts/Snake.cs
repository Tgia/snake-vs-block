﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Snake : LifeForObjs
{
    //length
    [SerializeField]
    public List<GameObject> snakeParts;
    //hp, partes
    [SerializeField]
    private GameObject part = null; // Body Snake
    [SerializeField]
    private int snakeLength = 0;
    private GameObject newPart; // Body Snake
    private void Awake()
    {
        addBodySnake();
    }

    private void addBodySnake()
    {
        for (int i = 0; i < snakeLength && snakeLength != snakeParts.Count; i++)
        {
            if (i == 0)
            {
                newPart = Instantiate(part, snakeParts[0].transform.position, Quaternion.identity);
            }
            else
            {
                newPart = Instantiate(part, new Vector3(snakeParts[i - 1].transform.position.x, snakeParts[i - 1].transform.position.y - 1f, 0f), Quaternion.identity);
            }
            snakeParts.Add(newPart);
        }
    }

    private void dropTail()
    {
        Debug.Log("Here, length: " + snakeLength.ToString());
        for (int i = snakeLength; i >= 0 && snakeLength != snakeParts.Count; i--)
        {
            Destroy(snakeParts[i]);
            snakeParts.Remove(snakeParts[i]);
        }
    }

   /* private void OnTriggerEnter2D(Collider2D collision)
    {
       convertionSucces = int.TryParse(playerLifeText.text, out snakeLength);
        if (willAdd)
        {
            addBodySnake();
        }
        else
        {
            dropTail();
        }
    }*/
}
