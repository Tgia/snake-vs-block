﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserInputForMovement : Snake
{
    public void gettingInput(float speed)
    {
        transform.position += new Vector3(Input.GetAxis("Horizontal"), speed * Time.deltaTime, 0);

        if (transform.position.x < -4)
        {
            Debug.Log("Less than 3");
            snakeParts[0].transform.position = new Vector3(-4, snakeParts[0].transform.position.y);
        }
        
        if (transform.position.x > 4)
        {
            Debug.Log("Greater than 3");
            snakeParts[0].transform.position = new Vector3(4, snakeParts[0].transform.position.y);
        }
    }
}
