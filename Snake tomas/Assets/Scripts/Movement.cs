﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;

public class Movement : UserInputForMovement
{
 
    [SerializeField]
    private float moveSpeed = 1f; //Speed in Y axis
    [SerializeField]
    private float speed = .1f; // Speed in X axis
    private Vector2 lastLocation;  // last location from Snake
    bool IsWait = false;
    Vector3 camera;

    private void Start()
    {
        camera = new Vector3(Camera.main.transform.position.x, transform.position.y, Camera.main.transform.position.z);

    }

    private void Update()
    {
        Move();
        if (IsWait)
        {
            moveSpeed = 0;
        }
    }
    
    //Snake Movement
    private void Move()
    {
        //Position for Snake Head
        snakeParts[0].transform.Translate(Vector2.up * moveSpeed * Time.deltaTime);
        lastLocation = snakeParts[0].transform.position;

        foreach (GameObject part in snakeParts)
        {
            if (part != snakeParts[0])
            {
                var newPosition = new Vector2(lastLocation.x, lastLocation.y - 0.4f);
                lastLocation = part.transform.position;
                part.transform.position = newPosition;
            }
        }
        gettingInput(speed);
    }
}
