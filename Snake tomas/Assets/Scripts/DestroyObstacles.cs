﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class DestroyObstacles : MonoBehaviour
{     
    private void OnTriggerEnter2D(Collider2D collision)
    {
        Invoke("waitToDestroy", .2f);
        
    }
    private void waitToDestroy()
    {
        Destroy(gameObject);
    }
}

