﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LifeForObjs : MonoBehaviour
{
    [SerializeField]
    private int firstRandomNumber = 0;
    [SerializeField]
    private int secondRandomNumber = 0;
    [SerializeField]
    private TextMesh lifeOfTheObject = null;
    private int CubeAndPickUpLifePoints = 0;
    private int savedLife = 0;
    [SerializeField]
    private int playerLifePoints = 0;
    [SerializeField]
    private GameObject player = null;
    private Color colourOfObject;
    public TextMesh playerLifeText;
    public bool convertionSucces = false;
    public bool generateOrRemoveTail = false;
    //GameObject player = null;

    private void Awake()
    {
        colourOfObject = this.GetComponent<SpriteRenderer>().color;
        playerLifeText = player.GetComponentInChildren<TextMesh>();
        convertionSucces = int.TryParse(playerLifeText.text, out playerLifePoints);
        Debug.Log("Value in player " + playerLifePoints.ToString());
        CubeAndPickUpLifePoints = setCubeAndPickUpLifePoints(colourOfObject);
        lifeOfTheObject.text = CubeAndPickUpLifePoints.ToString();

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (colourOfObject == Color.white)
        {
            Debug.Log("DETECTED REMOVE");
            deleteLifePoints();
        }
        else
        {
            addLifePoints();
            Debug.Log("DETECTED ADD");
        }
    }


    private int setCubeAndPickUpLifePoints(Color colourOfObject)
    {
        int lifePointsSetted = 0;
        lifePointsSetted = Random.Range(firstRandomNumber, secondRandomNumber);
        Debug.Log("LIFEPOINTS OBJ: " + lifePointsSetted.ToString());
        return lifePointsSetted;
    }

    private void addLifePoints()
    {
        generateOrRemoveTail = true;
        Debug.Log("lifePoints pre add: " + CubeAndPickUpLifePoints.ToString());
        Debug.Log("playerlife pre add: " + playerLifePoints.ToString());
        playerLifeText.text = (CubeAndPickUpLifePoints + playerLifePoints).ToString();
        playerLifePoints = CubeAndPickUpLifePoints + playerLifePoints;
        Debug.Log("lifePoints post add: " + CubeAndPickUpLifePoints.ToString());
        Debug.Log("playerlife post add: " + playerLifePoints.ToString());
    }
    private void deleteLifePoints()
    {
        
        if (playerLifePoints > CubeAndPickUpLifePoints)
        {
            CubeAndPickUpLifePoints = CubeAndPickUpLifePoints - playerLifePoints;
            playerLifePoints = CubeAndPickUpLifePoints;
            playerLifeText.text = CubeAndPickUpLifePoints.ToString();
        }
        if(playerLifePoints < CubeAndPickUpLifePoints)
        {
            CubeAndPickUpLifePoints = 0;
            playerLifePoints = CubeAndPickUpLifePoints;
            playerLifeText.text = CubeAndPickUpLifePoints.ToString();
            lifeOfTheObject.text = (CubeAndPickUpLifePoints - playerLifePoints).ToString();
            if (playerLifePoints == 0) {
                SceneManager.LoadScene("Lose");
            }
        }
    }
    private int setSavedPlayerLife(int savedLife)
    {
        playerLifePoints = savedLife;
        return playerLifePoints;
    }
}


